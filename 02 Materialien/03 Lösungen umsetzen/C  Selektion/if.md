# `if`-Selektion

Mit einer `if`-Selektion können Anweisungen in Abhängigkeit einer Bedingung ausgeführt werden. Wenn die Bedingung erfüllt ist, d.h. die Auswertung der Bedingung den boolschen Wert `true` ergibt, werden die nachfolgend aufgeführten Anweisungen ausgeführt. Umgekehrt, wenn die Auswertung der Bedingung den boolschen Wert `false` ergibt, werden die nachfolgend aufgeführten Anweisungen *nicht* ausgeführt.

```c#
if (Bedingung)
{
    Anweisungen
}
```

Die *Bedingung* der `if`-Selektion wird nach dem `if` Schlüsselwort in runden Klammern geschrieben. Dabei kann diese Bedingung beispielsweise eine Variable vom Datentyp `bool` sein. Häufiger werden jedoch Ausdrücke verwendet, welche als Ergebnis einen boolschen Wahrheitswert (`true` oder `false`) haben. Meist werden dazu Vergleichsoperatoren wie `>`, `<`, `>=`, `<=`, `==` oder `!=` verwendet.

Die selektiv auszuführenden Anweisungen werden nach der Bedingung zwischen geschweifte Klammern `{` und `}` geschrieben. Wenn die Selektion lediglich eine Anweisung betrifft, könnte diese Anweisung auch direkt nach der Bedingung, ohne geschweifte Klammern, geschrieben werden. Von dieser Notation wird jedoch abgeraten, da sie gerade bei Programmieranfängerinnen und -anfängern schneller zu unübersichtlichen Code-Stellen oder Fehlern führen kann.

```c#
// Declare a variable for the result of an integer division.
int result;

// Check if the divisor is different from zero.
if (divisor != 0)
{
    // Assupmtion: The variables dividend and divisor
    //			   were defined somwhere else.
    result = dividend / divisor;
}
```

