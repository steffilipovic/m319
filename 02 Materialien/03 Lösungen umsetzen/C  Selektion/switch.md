# `switch`-Selektion

Mit einer `switch`-Selektion kann aus einer Liste beliebig vieler Fälle (`switch`-Sektionen) ein einziger, passender Fall ausgewählt und die entsprechenden Anweisungen ausgeführt werden. Dazu wird nach dem Schlüsselwort `switch` ein zu überprüfender Ausdruck in runden klammern notiert. Jede `switch`-Sektion wird mit (mindestens) einem Schlüsselwort `case` und einer zugehörigen Anweisung gekennzeichnet. Beendet wird jede `switch`-Sektion mit einer sogenannten "Sprunganweisung" - meistens mit dem Schlüsselwort `break`.

Es wird immer die erste `switch`-Sektion ausgeführt, deren Anweisung nach dem `case`-Schlüsselwort zum Ausdruck der `switch`-Selektion passt. Die Reihenfolge der Fälle (`case`) spielt folglich eine Rolle.

```c#
switch (Ausdruck)
{
    case Anweisung1:
        // switch-Sektion 1
        break;
    case Anweisung2:
        // switch-Sektion 2
        break;
    default:
        // Standard-Sektion
        break;
}
```

Für die Situation, dass keine der aufgeführten Fälle (`case`) zutreffen, kann ein "Standardfall" mit dem Schlüsselwort `default` spezifiziert werden. Die Position der `switch`-Sektion mit dem `default`-Schlüsselwort innerhalb der Liste aller `switch`-Sektionen ist irrelevant - es werden immer zuerst alle "normalen" Fälle geprüft.

Es ist auch möglich, für eine `switch`-Sektion mehrere Fälle mit dem Schlüsselwort `case` aufzuführen. Dabei reicht es, wenn *eine* der aufgeführten Anweisungen zum Ausdruck der `switch`-Selektion passt.

```c#
// Declare a variable to save the number of days in a month.
int days;

// Day number of days depends on the month.
switch (month)
{
    // February has only 28 days.
    case 2:
        days = 28;
        break;

    // April, june, september and november have 30 days.
    case 4:
    case 6:
    case 9:
    case 11:
        days = 30;
        break;
        
    // All other months have 31 days.    
    default:
        days = 31;
        break;
}
```
